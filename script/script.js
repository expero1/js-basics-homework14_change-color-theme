const darkThemeClass = "dark";
const changeThemeBtn = document.getElementById("change-theme-btn");
const changeTheme = () => {
  document.body.classList.toggle(darkThemeClass);
  localStorage.darkTheme = JSON.stringify(
    document.body.classList.contains(darkThemeClass)
  );
};
changeThemeBtn.addEventListener("click", changeTheme);
const darkTheme = JSON.parse(localStorage.darkTheme ?? "false");
if (darkTheme) {
  document.body.classList.add(darkThemeClass);
} else {
  document.body.classList.remove(darkThemeClass);
}
